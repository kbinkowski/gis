package view;

import java.util.List;

import javax.swing.JPanel;

import model.Route;

public class RouteView{

	JPanel workArea;
	Route route;
	List<CityView> cities;

	List<String> roads;

	String description;

	public RouteView(List<CityView> cities, List<String> roads, Route route) {
		this.route = route;
		this.cities = cities;
		this.roads = roads;
		setDescription();
	}

	private void setDescription() {
		StringBuilder sb = new StringBuilder();
		sb = sb.append("\nTrasa:\t");
		
		if(cities.isEmpty() || roads.isEmpty() || route == null){
			sb = sb.append("\nTyp:\t\nOdległość:\t\nCzas:\t");
			description = sb.toString();
			return;
		}
		
		int time = route.getTime();
		int h = (int) Math.floorDiv(time, 60);
		int m = (int) Math.floorMod(time, 60);
		
		CityView tempCity = cities.get(0);
		String tempRoad;
		sb.append(tempCity.toString());
		
		for(int i=0; i<roads.size(); ++i){
			tempCity = cities.get(i+1);
			tempRoad = roads.get(i);
			sb = sb.append(" -- ")
					.append(tempRoad)
					.append(" -> ")
					.append(tempCity);
		}
		
		description = sb.append("\nTyp:\t")
					.append(route.getType())
					.append("\nOdległość:\t")
					.append(route.getDistance())
					.append(" km\nCzas:\t")
					.append(h)
					.append(" h ")
					.append(m)
					.append(" m")
					.toString();
	}
	
	public List<CityView> getCities() {
		return cities;
	}	

	@Override
	public String toString() {
		return description;
	}

}
