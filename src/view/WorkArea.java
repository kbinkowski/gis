package view;

import static view.Properties.MAP_PATH;
import static view.Properties.ROUTE;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class WorkArea extends JPanel {

	private static final long serialVersionUID = 1L;

	private Image background;
	private ArrayList<CityView> cities;
	private RouteView route;

	
	public WorkArea() {
		try {
			this.background = ImageIO.read(getClass().getResource(MAP_PATH));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	void setCities(ArrayList<CityView> cities) {
		this.cities = cities;
		for (CityView city : cities) {
			add(city);
		}
	}
	
	void setRoute(RouteView route){
		this.route = route;
	}
	
	public void update(){
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// Map
		g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		// Cities
		if (cities != null)
			for (CityView city : cities) {
				city.update();
			}
		// Route
		if (route != null){
			List<CityView> routeCities = route.getCities();
			for(int i=0; i<routeCities.size()-1; ++i){
				CityView cityA = routeCities.get(i);
				CityView cityB = routeCities.get(i+1);
				g.setColor(ROUTE);
				g.drawLine(cityA.getLocationX(), cityA.getLocationY(), cityB.getLocationX(), cityB.getLocationY());
			}
		}
	}

}
