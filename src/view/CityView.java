package view;

import static view.Properties.CITY_SIZE_PCT;
import static view.Properties.NEUTRAL_CITY;

import static common.GeoProperties.MAX_LATITUDE;
import static common.GeoProperties.MIN_LONGITUDE;
import static common.GeoProperties.SPAN_LATITUDE;
import static common.GeoProperties.SPAN_LONGITUDE;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JPanel;

import common.GeoUtil;

import model.City;

public class CityView extends JButton {

	private static final long serialVersionUID = 1L;

	private JPanel workArea;
	private City city;

	private Color cityTypeColor = NEUTRAL_CITY;
	private int locationX;
	private int locationY;
	private int sizeX;
	private int sizeY;

	CityView(JPanel workArea, City city) {
		this.workArea = workArea;
		this.city = city;

		setToolTipText(city.getName());
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setOpaque(false);
	}

	void update() {
		calculateSize();
		calculateLocation();
		setBounds(locationX - sizeX / 2, locationY - sizeY / 2, sizeX, sizeY);
		repaint();
	}

	private void calculateSize() {
		sizeX = (int) Math.round(workArea.getWidth() * CITY_SIZE_PCT);
		sizeY = (int) Math.round(workArea.getHeight() * CITY_SIZE_PCT);
	}

	private void calculateLocation() {
		locationX = (int) Math.round(GeoUtil.calculateDistanceX(MIN_LONGITUDE, city.getLongitude())
				* workArea.getWidth() / SPAN_LONGITUDE)
				+ sizeX / 2;
		locationY = (int) Math.round(GeoUtil.calculateDistanceY(city.getLatitude(), MAX_LATITUDE)
				* workArea.getHeight() / SPAN_LATITUDE)
				+ sizeY / 2;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(cityTypeColor);
		g.fillOval(0, 0, sizeX, sizeY);
	}

	public int getLocationX() {
		return locationX;
	}

	public int getLocationY() {
		return locationY;
	}

	public String getName() {
		return city.getName();
	}

	public int getId() {
		return city.getId();
	}

	public Color getType() {
		return cityTypeColor;
	}

	public void setType(Color cityTypeColor) {
		this.cityTypeColor = cityTypeColor;
	}

	public City getCity() {
		return city;
	}

	@Override
	public String toString() {
		return city.toString();
	}

}
