package view;

import java.awt.Color;

public interface Properties {

	String MAP_PATH = "/resources/Polska.jpg";
	
	Color NEUTRAL_CITY = Color.YELLOW;
	Color START_CITY = Color.GREEN;
	Color END_CITY = Color.RED;
	Color ROUTE = Color.BLUE;
	
	Double CITY_SIZE_PCT = 0.02;
}
