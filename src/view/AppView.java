package view;

import static common.PathType.DISTANCE_TYPE;
import static common.PathType.TIME_TYPE;
import static view.Properties.END_CITY;
import static view.Properties.NEUTRAL_CITY;
import static view.Properties.START_CITY;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

import model.City;
import model.Road;
import model.Route;
import controler.AppControler;

public class AppView extends JFrame {

	private static final long serialVersionUID = 1L;

	// Functional elements
	AppControler controler;

	ArrayList<CityView> cities = new ArrayList<CityView>();
	CityView startCity;
	CityView endCity;
	RouteView route;
	RouteView nullRoute;

	// View components
	Dimension defaultDimension = new Dimension(1000, 900);
	JPanel confPanel;
	WorkArea workArea;
	JPanel resultPanel;
	JTextArea resultArea;
	JComboBox<String> startCitySelect;
	JComboBox<String> endCitySelect;
	ButtonGroup pathTypeSelect;
	JRadioButton typeDistance;
	JRadioButton typeTime;
	JButton submit;

	public AppView(AppControler controler) {
		super("PathFinder");
		this.controler = controler;

		setPreferredSize(defaultDimension);
		init();
		pack();
		setVisible(true);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		
		nullRoute = new RouteView(new ArrayList<CityView>(), new ArrayList<String>(), null);
		setRouteView(null);
	}

	public void setCityViews(Collection<City> collection) {
		for (City city : collection)
			addCityView(city);
		workArea.setCities(cities);
	}

	public void addCityView(final City city) {
		final CityView cityView = new CityView(workArea, city);
		cityView.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String text = "Ustaw miasto " + city.getName() + " jako:";
				Object[] options = { "START", "KONIEC" };
				int result = JOptionPane.showOptionDialog(workArea, text, "", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

				if (result == 0)
					setStartCity(cityView.getId());
				else if (result == 1)
					setEndCity(cityView.getId());
			}

		});

		cities.add(cityView);
		endCitySelect.addItem(city.getName());
		startCitySelect.addItem(city.getName());
	}

	public void setStartCity(int id) {
		if (cities == null)
			return;

		CityView oldCity = startCity;
		if (id < 0)
			return;

		CityView newCity = cities.get(id);

		if (newCity == oldCity)
			return;

		if (newCity != null) {
			startCity = newCity;
			newCity.setType(START_CITY);
			startCitySelect.setSelectedIndex(id);
			if (oldCity != null)
				oldCity.setType(NEUTRAL_CITY);
			if (newCity == endCity) {
				endCity = null;
				endCitySelect.setSelectedIndex(-1);
			}
			setRouteView(null);
		}
	}

	public void setEndCity(int id) {
		if (cities == null)
			return;

		CityView oldCity = endCity;
		if (id < 0)
			return;
		
		CityView newCity = cities.get(id);

		if (newCity == oldCity)
			return;

		if (newCity != null) {
			endCity = newCity;
			newCity.setType(END_CITY);
			endCitySelect.setSelectedIndex(id);
			if (oldCity != null)
				oldCity.setType(NEUTRAL_CITY);
			if (newCity == startCity) {
				startCity = null;
				startCitySelect.setSelectedIndex(-1);
			}
			setRouteView(null);
		}
	}

	public void setRouteView(Route route){
		
		if(route == null){
			this.route = this.nullRoute;
			updateView();
			return;
		}
			
		ArrayList<CityView> routeCityViews = new ArrayList<CityView>();
		ArrayList<String> routeRoadViews = new ArrayList<String>();
		
		List<City> routeCities = route.getCities();
		List<Road> routeRoads = route.getRoads();
		
		if(routeCities.isEmpty() || routeRoads.isEmpty())
			return;
		
		City tempCity = routeCities.get(0);
		CityView tempCityView = cities.get(tempCity.getId());
		Road tempRoad;
		
		routeCityViews.add(tempCityView);
		
		for(int i=0; i<routeRoads.size(); ++i){
			tempCity = routeCities.get(i+1);
			tempCityView = cities.get(tempCity.getId());	
			routeCityViews.add(tempCityView);
			tempRoad = routeRoads.get(i);
			routeRoadViews.add(tempRoad.getDescriptionTo(tempCity));
		}
		
		this.route = new RouteView(routeCityViews, routeRoadViews, route);
		updateView();
	}
	
	private void updateView(){
		workArea.setRoute(this.route);	
		workArea.update();
		resultArea.setText(this.route.toString());
	}

	private void init() {
		initConfPanel();
		initWorkPanel();
		initResultPanel();

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = gbc.gridy = 0;
		gbc.gridwidth = gbc.gridheight = 1;
		gbc.weightx = gbc.weighty = 90;
		gbc.fill = GridBagConstraints.BOTH;
		add(workArea, gbc);

		gbc.gridy = 1;
		gbc.weightx = gbc.weighty = 10;
		add(resultPanel, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 2;
		add(confPanel, gbc);
	}

	private void initConfPanel() {
		confPanel = new JPanel(new GridLayout(11, 1));
		confPanel.setBorder(BorderFactory.createTitledBorder("Konfiguracja"));

		startCitySelect = new JComboBox<String>();
		startCitySelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setStartCity(startCitySelect.getSelectedIndex());
			}
		});

		endCitySelect = new JComboBox<String>();
		endCitySelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setEndCity(endCitySelect.getSelectedIndex());
			}
		});

		pathTypeSelect = new ButtonGroup();
		typeDistance = new JRadioButton("Odległość");
		typeTime = new JRadioButton("Czas");
		typeDistance.setSelected(true);
		pathTypeSelect.add(typeDistance);
		pathTypeSelect.add(typeTime);

		submit = new JButton("Szukaj");
		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (startCity == null || endCity == null) {
					JOptionPane.showMessageDialog(workArea, "Proszę wybrać miasta początku i końca trasy", "Uwaga",
							JOptionPane.WARNING_MESSAGE);
					return;
				}
				Route result;
				if (typeDistance.isSelected())
					result = controler.searchRoute(startCity.getCity(), endCity.getCity(), DISTANCE_TYPE);
				else
					result = controler.searchRoute(startCity.getCity(), endCity.getCity(), TIME_TYPE);
				setRouteView(result);
			}
		});

		confPanel.add(new JLabel("Miasto początkowe:"));
		confPanel.add(startCitySelect);
		confPanel.add(new JLabel());
		confPanel.add(new JLabel("Miasto końcowe:"));
		confPanel.add(endCitySelect);
		confPanel.add(new JLabel());
		confPanel.add(new JLabel("Typ trasy:"));
		confPanel.add(typeDistance);
		confPanel.add(typeTime);
		confPanel.add(new JLabel());
		confPanel.add(submit);
	}

	private void initWorkPanel() {
		workArea = new WorkArea();
		workArea.setLayout(null);
	}

	private void initResultPanel() {
		resultPanel = new JPanel(new BorderLayout());
		resultPanel.setBorder(BorderFactory.createTitledBorder("Wynik"));
		resultArea = new JTextArea();
		resultArea.setLineWrap(true);
		resultArea.setBackground(getBackground());
		resultPanel.add(resultArea, BorderLayout.CENTER);
	}

}
