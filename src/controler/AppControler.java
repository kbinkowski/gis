package controler;

import java.util.Collection;

import common.PathType;
import model.AppModel;
import model.City;
import model.Route;
import view.AppView;

public class AppControler {
	
	private AppModel model;
	private AppView view;
	
	AppControler(){
		createView();
		createModel();
	}
	
	private void createModel(){
		model = new AppModel(this);
	}
	
	private void createView(){
		final AppControler that = this;
		try {
			javax.swing.SwingUtilities.invokeAndWait( new Runnable() {
				public void run() {
					try { 
						view = new AppView(that); 
					} 
					catch (Exception e) {
						System.err.println("Error creating AppView: " + e);
						e.printStackTrace();
			            System.exit(1);
					}
			    }
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	public void updateCities(Collection<City> collection) {
		view.setCityViews(collection);
	}
	
	public Route searchRoute(City start, City end, PathType type){
		return model.searchRoute(start, end, type);
	}
	
	public static void main(String args[]){
		new AppControler();
	}
}
