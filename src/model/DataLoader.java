package model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DataLoader {
	String CITIES_PATH = "./conf/cities.txt";
	String ROADS_PATH = "./conf/roads.txt";
		
	private Map<String,City> citiesByName;
	private ArrayList<City> cities;
	private Map<Integer,Road> roadsById;
	
	public DataLoader( ) throws NumberFormatException, IOException {
		loadCities( );
		loadRoads( );
	}

	private void loadCities( ) throws NumberFormatException, IOException{
		citiesByName = new HashMap<String,City>();
		cities = new ArrayList<City>();
		
		BufferedReader streamCities;
		
		streamCities = new BufferedReader(new InputStreamReader(new FileInputStream(CITIES_PATH), "UTF-8"));
	
		String line = streamCities.readLine();
		int idx = 0;
		while( (line=streamCities.readLine()) != null ){
			String[] tab = line.split(";");
			if(tab.length>=3){
				Double longitude = ParseDoubleWithComma(tab[1]);
				Double latitude = ParseDoubleWithComma(tab[2]);
				City city = new City(idx, tab[0], longitude, latitude);
				citiesByName.put(tab[0], city);
				cities.add(city);
				++idx;
			}
		}
		streamCities.close();
	}

	private void loadRoads() throws NumberFormatException, IOException {
		roadsById = new HashMap<Integer, Road>();
		
		BufferedReader streamRoads;
		
		streamRoads = new BufferedReader(new InputStreamReader(new FileInputStream(ROADS_PATH), "UTF-8"));
	
		String line = streamRoads.readLine();

		while( (line = streamRoads.readLine()) != null ){
			String[] tab = line.split(";");
			if(tab.length>=5){
				Double length = ParseDoubleWithComma(tab[2]);
				String description = tab[4];
				String[] timeParts = tab[3].split(":");
				int time = Integer.valueOf(timeParts[0])*60 + Integer.valueOf(timeParts[1]);
				City cityA = citiesByName.get(tab[0]);
				City cityB = citiesByName.get(tab[1]);
				int id = Road.getHashId(cityA.getId(), cityB.getId());
				Road road = new Road(description, id, length, time, cityA, cityB );
				roadsById.put(id, road);
			}
		}
		streamRoads.close();
	}
	
	public ArrayList<City> getCities( )
	{
		return this.cities;
	}

	
	public Map<Integer, Road> getRoads( )
	{
		return this.roadsById;
	}
	
	private double ParseDoubleWithComma( String string )
	{
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		Number number;
		try {
			number = format.parse(string);
		    return number.doubleValue();

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
