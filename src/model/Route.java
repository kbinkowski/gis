package model;

import java.util.List;

import common.PathType;

public class Route {
	
	List<Road> roads;
	List<City> cities;
	
	double distance; //w kilometrach
	int time; //w minutach
	PathType type;
	
	
	public Route(List<City> cities, List<Road> roads, double distance, int time, PathType type){
		this.roads = roads;
		this.cities = cities;
		this.time = time;
		this.distance = distance;
		this.type = type;
	}
	
	
	public double getDistance() {
		return distance;
	}
	
	public int getTime() {
		return time;
	}
	
	public PathType getType() {
		return type;
	}

	public List<City> getCities() {
		return cities;
	}
	
	public List<Road> getRoads() {
		return roads;
	}
	
}
