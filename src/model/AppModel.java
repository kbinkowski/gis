package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import astar.AStarAlgorithm;
import common.PathType;
import controler.AppControler;

public class AppModel {
	AppControler controler;
	AStarAlgorithm algorithm;
	ArrayList<City> cities = new ArrayList<City>();
	Map<Integer, Road> roads = new HashMap<Integer, Road>(); //integer to id trasy, najlepiej jakiś unikalny hash z id miast krańcowych, by łatwiej wyszukiwać

	public AppModel(AppControler controler) {
		this.controler = controler;

		DataLoader dataLoader;
		try {
			dataLoader = new DataLoader();
			cities = dataLoader.getCities();
			controler.updateCities(cities);
			roads = dataLoader.getRoads();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
			return;
		}
		
		algorithm = new AStarAlgorithm(cities, roads);
	}

	public Route searchRoute(City start, City end, PathType type) {
		return algorithm.searchRoute(start, end, type);
	}
}
