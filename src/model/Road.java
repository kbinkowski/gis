package model;

import static model.Properties.MAX_NUMBER_OF_CITY;

public class Road {
	String descriptionToA;
	String descriptionToB;
	int id;
	double length;
	int time; 
	City cityA;
	City cityB;
	
	public static int getHashId(int id1, int id2){
		if (id1<id2)
			return id1*MAX_NUMBER_OF_CITY + id2;
		else
			return id2*MAX_NUMBER_OF_CITY + id1;
	}
	
	public Road(String description, int id, double length, int time, City cityA, City cityB) {
		this.id = id;
		this.length = length;
		this.time = time;
		this.setCityA(cityA);
		this.setCityB(cityB);
		setDescription(description);
	}
	
	private void setDescription(String description) {
		String[] tab = description.split(" - ");
		StringBuilder sb = new StringBuilder();
		
		if(tab.length > 1)
			for(int i=tab.length-1; i>0; --i)
				sb = sb.append(tab[i]).append(" - ");
		
		descriptionToA = sb.append(tab[0]).toString(); 	
		descriptionToB = description;
	}

	public int getId() {
		return id;
	}
	

	public String getDescriptionTo(City city) {
		if(city.getId() == cityA.getId())
			return descriptionToA;
		else
			return descriptionToB;
	}

	public double getLength() {
		return length;
	}

	public int getTime() {
		return time;
	}
	

	public City getCityA() {
		return cityA;
	}

	public void setCityA(City cityA) {
		this.cityA = cityA;
	}

	public City getCityB() {
		return cityB;
	}

	public void setCityB(City cityB) {
		this.cityB = cityB;
	}
	
	public boolean goesThroughCity(int cityId)
	{
		return (this.cityA.id == cityId || this.cityB.id == cityId );
	}
}
