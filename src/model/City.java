package model;

public class City {

	protected int id;
	protected String name; 
	protected double longitude; // długość geograficzna (E-W) w stopniach

	protected double latitude; // szerokość geograficzna (N-S) w stopniach 
	

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public City(int id, String name, double longitude, double latitude){
		this.id = id;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
