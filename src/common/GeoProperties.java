package common;

public interface GeoProperties {
	
	double RADIUS_EARTH = 6378.41;
	double MEAN_LATITUDE = 51.91;
	
	double MIN_LATITUDE = 49.00;
	double MAX_LATITUDE = 54.83;
	double MIN_LONGITUDE = 14.12;
	double MAX_LONGITUDE = 24.13;
	
	double SPAN_LATITUDE = (MAX_LATITUDE-MIN_LATITUDE) * Math.PI *  RADIUS_EARTH / 360;
	double SPAN_LONGITUDE = (MAX_LONGITUDE-MIN_LONGITUDE) * Math.cos( MEAN_LATITUDE * Math.PI/180 ) * Math.PI *  RADIUS_EARTH / 360; 
	
}
