package common;

public enum PathType {	
	DISTANCE_TYPE("Najkrótszy dystans"),
	TIME_TYPE("Najkrótszy czas")
	;
	
	private final String description;
	
	private PathType(final String description){
		this.description = description;
	}
	
	@Override
    public String toString() {
        return description;
    }
}
