package common;

import static common.GeoProperties.MEAN_LATITUDE;
import static common.GeoProperties.RADIUS_EARTH;

public class GeoUtil {

	public static double calculateDistance(double Lat1, double Lon1, double Lat2, double Lon2){
		double a = (Lon2-Lon1) * Math.cos( Lat1 * Math.PI/180 );
		double b = (Lat2-Lat1);
		return Math.sqrt(a*a+b*b) * Math.PI *  RADIUS_EARTH / 360;		
	}
	
	public static double calculateDistanceX(double Lon1, double Lon2){
		return (Lon2-Lon1) * Math.cos( MEAN_LATITUDE * Math.PI/180 ) * Math.PI *  RADIUS_EARTH / 360;
	}
	
	public static double calculateDistanceY(double Lat1, double Lat2){
		return (Lat2-Lat1) * Math.PI *  RADIUS_EARTH / 360;
	}
}
