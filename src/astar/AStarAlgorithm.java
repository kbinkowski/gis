package astar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import model.City;
import model.Properties;
import model.Road;
import model.Route;
import common.GeoUtil;
import common.PathType;

public class AStarAlgorithm {
	private ArrayList<AStarCity> cities = new ArrayList<AStarCity>();
	private Map<Integer, Road> roads = new HashMap<Integer, Road>();  
	
	private AStarCity start;
	private AStarCity end;
	private PriorityQueue<AStarCity> opened;
	private HashSet<City> closed;
	private PathType type;

	public AStarAlgorithm(ArrayList<City> cities, Map<Integer, Road> roadsMap )
	{
		for (City city : cities) {
			AStarCity aStarCity = new AStarCity(city);
			aStarCity.ResolveNeighborsList(roadsMap.values());
			this.cities.add( aStarCity );
		}
		roads = roadsMap;
	}
	
	public Route searchRoute(City startCity, City endCity, PathType type) {
		InitSearch( );

		start = cities.get(startCity.getId());
		end = cities.get(endCity.getId());
		this.type = type;
		
		opened.add(start);
		
		while (opened.size() > 0) 
		{
			/*for(AStarCity a : opened)
				System.out.println(a.getName() + " " + a.getF());
			System.out.println( );*/
			AStarCity Q = opened.poll();
			closed.add(Q);
			
			if (Q.getId() == endCity.getId())
			{
				return GenerateResult(Q);
			}
			for(Integer neighborId : Q.getNeighbors())
			{
				AStarCity neighbor = cities.get(neighborId);
				if (!closed.contains(neighbor))
				{
					if (!opened.contains(neighbor))
					{
						neighbor.setPredecessorId(Q.getId());
						neighbor.setG(calculateGValue(neighbor, Q.getId()));
						neighbor.setF(calculateFValue(neighbor));
						opened.add(neighbor);
					}
					else
					{
						double newG = calculateGValue(neighbor, Q.getId());
						if (newG < neighbor.getG())
						{
							neighbor.setPredecessorId(Q.getId());
							neighbor.setG(newG);
							neighbor.setF(calculateFValue(neighbor));
							opened.remove(neighbor);
							opened.add(neighbor);
						}
					}
				}
			}
		}
		
		return null;
	}
	
	private double calculateGValue(AStarCity city, Integer predecessorId) {
		Road road = roads.get(Road.getHashId(predecessorId, city.getId()));
		
		if (type == PathType.DISTANCE_TYPE)
			return cities.get(predecessorId).getG() + road.getLength();
		else
			return cities.get(predecessorId).getG() + road.getTime();			
	}
	
	private double calculateFValue(AStarCity city) {
		return city.getG() + getHeuristic( city );
	}	

	private double getHeuristic(AStarCity city) {
		double H;
		if (type == PathType.DISTANCE_TYPE)
			H = GeoUtil.calculateDistance(city.getLatitude(), city.getLongitude(), end.getLatitude(), end.getLongitude());
		else
			H = 60*GeoUtil.calculateDistance(city.getLatitude(), city.getLongitude(), end.getLatitude(), end.getLongitude())/Properties.MAXIMUM_AVERAGE_SPEED;
		city.setH(H);
		return H;
	}

	private void InitSearch( )
	{
		opened = new PriorityQueue<AStarCity>( cities.size(), new Comparator<AStarCity>() {
			@Override
			public int compare(AStarCity o1, AStarCity o2) {
				return (int) (o1.getF() - o2.getF());
			}
			
		} );
		closed = new HashSet<City>();

		for( AStarCity city : cities )
			city.RestoreDefaults( );
	}
	
	Route GenerateResult( AStarCity last )
	{
		List<City> routeCities = new ArrayList<City>( );
		
		Integer predecessorId = last.getPredecessorId(); 
		routeCities.add(last);
		while (predecessorId != null)
		{
			last = cities.get(predecessorId);
			routeCities.add(last);
			predecessorId = last.getPredecessorId();
		}
		
		Collections.reverse(routeCities);
		
		List<Road> routeRoads = GenerateRouteRoads( routeCities );
		
		double overallDistance = CalculateOverallDistance( routeRoads );
		int overallTime = CalculateOverallTime( routeRoads );
		
		return new Route(routeCities, routeRoads, overallDistance, overallTime, type);
	}

	private int CalculateOverallTime(List<Road> routeRoads) {
		int time = 0;
		
		for (Road road : routeRoads)
			time += road.getTime();
		
		return time;
	}

	private double CalculateOverallDistance(List<Road> routeRoads) {
		int distance = 0;
		
		for (Road road : routeRoads)
			distance += road.getLength();
		
		return distance;
	}

	private List<Road> GenerateRouteRoads(List<City> routeCities) {
		List<Road> routeRoads = new ArrayList<Road>( routeCities.size() - 1 );
		City A = routeCities.get(0);
		City B;
		for (City city : routeCities.subList(1, routeCities.size())) {
			B = city;
			routeRoads.add(roads.get(Road.getHashId(A.getId(), B.getId())));
			A = city;
		}
		return routeRoads;
	}

}
