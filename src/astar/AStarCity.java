package astar;

import java.util.ArrayList;
import java.util.Collection;

import model.City;
import model.Road;

public class AStarCity extends City {
	private double F;
	private double G;
	private double H;
	private Integer PredecessorId;
	private ArrayList<Integer> NeighborsIds;
	
	public AStarCity(int id, String name, double longitude, double latitude) {
		super(id, name, longitude, latitude);
	}

	public AStarCity(City city) {
		super(city.getId(), city.getName(), city.getLongitude(), city.getLatitude());
	}
	
	public void ResolveNeighborsList( Collection<Road> collection )
	{
		NeighborsIds = new ArrayList<Integer>( );
		
		for (Road road : collection) {
			if (road.getCityA().getId() == this.id)
				NeighborsIds.add(road.getCityB().getId());
			else if (road.getCityB().getId() == this.id)
				NeighborsIds.add(road.getCityA().getId());
		}
	}

	public void RestoreDefaults() {
		setPredecessorId(null);
		setF(setG(setH(0)));
	}

	double getF() {
		return F;
	}

	void setF(double f) {
		F = f;
	}

	double getG() {
		return G;
	}

	double setG(double g) {
		G = g;
		return g;
	}

	double getH() {
		return H;
	}

	double setH(double h) {
		H = h;
		return h;
	}

	Integer getPredecessorId() {
		return PredecessorId;
	}

	void setPredecessorId(Integer predecessorId) {
		PredecessorId = predecessorId;
	}

	ArrayList<Integer> getNeighbors() {
		return NeighborsIds;
	}

	void setNeighbors(ArrayList<Integer> neighbors) {
		NeighborsIds = neighbors;
	}
}
