Miasto A;Miasto B;Długość;Czas;Opis
Warszawa;Łódź;130;01:37;A2 
Warszawa;Lublin;169;02:14;DK17
Warszawa;Białystok;192;02:28;S8 - DK8
Warszawa;Radom;105;01:24;S7 - DK7
Warszawa;Toruń;212;02:57;DK10
Warszawa;Olsztyn;214;02:37;DK7
Warszawa;Elbląg;285;03:18;DK7 - S7
Warszawa;Płock;110;01:33;DK62
Warszawa;Siedlce;92;01:26;DK2
Warszawa;Łomża;138;01:56;S8 - DW677
Kraków;Katowice;79;00:59;A4
Kraków;Kielce;116;01:49;S7
Kraków;Bielsko-Biała;124;01:34;A4 - DK1
Kraków;Tarnów;83;01:06;A4
Kraków;Nowy Sącz;111;01:39;A4 - DK75
Łódź;Wrocław;221;02:12;S8 - A8
Łódź;Lublin;288;03:37;A2 - DK50 - S7 - DK48 - DK12 - DK17 
Łódź;Radom;182;02:11;A2 - DK50 - E77
Łódź;Toruń;174;01:54;A1
Łódź;Płock;113;01:31;A1 - DK60
Łódź;Opole;213;02:36;S8 - DK11 - DK45
Łódź;Włocławek;121;01:24;A1 - DK91
Łódź;Kalisz;121;01:37;S8 - DK12
Łódź;Konin;110;01:22;A2
Łódź;Piotrków Trybunalski;49;00:49;DK1 - A1
Wrocław;Poznań;175;02:40;DK5
Wrocław;Częstochowa;194;02:33;S8 - DK43
Wrocław;Opole;100;01:13;A4
Wrocław;Zielona Góra;157;02:18;DK94 - S3
Wrocław;Kalisz;118;01:54;DK25
Wrocław;Legnica;78;01:01;A4
Wrocław;Jelenia Góra;112;01:39;DK5
Wrocław;Piotrków Trybunalski;224;02:22;S8
Poznań;Bydgoszcz;142;01:56;DK5
Poznań;Toruń;162;02:23;DK15
Poznań;Gorzów Wielkopolski;163;01:39;A2 - S3
Poznań;Zielona Góra;153;01:37;A2 - A3
Poznań;Kalisz;143;01:38;A2 - DK25
Poznań;Legnica;181;02:43;DW434 - DK36
Poznań;Konin;107;01:09;A2
Poznań;Piła;97;01:40;DW178
Gdańsk;Gdynia;22;00:33;DW468
Gdańsk;Elbląg;61;00:51;DK7
Gdańsk;Grudziądz;109;01:13;A1
Gdańsk;Słupsk;125;01:55;DK6
Gdańsk;Piła;255;03:08;A1 - DK10
Szczecin;Gorzów Wielkopolski;105;01:11;S3
Szczecin;Koszalin;161;02:06;DK6
Szczecin;Piła;170;02:22;DK10
Bydgoszcz;Toruń;46;00:47;DK80
Bydgoszcz;Koszalin;193;02:56;DK25
Bydgoszcz;Grudziądz;72;01:05;DK5
Bydgoszcz;Słupsk;202;03:01;DK25 - DK21
Bydgoszcz;Konin;113;01:52;DK25
Bydgoszcz;Piła;89;01:25;DK10
Lublin;Białystok;247;03:25;DK19
Lublin;Radom;117;01:37;DK12
Lublin;Kielce;174;02:36;DK74
Lublin;Rzeszów;162;02:29;DK19
Lublin;Siedlce;123;01:52;DK19 - DK63
Lublin;Zamość;88;01:11;DK17
Katowice;Częstochowa;74;01:02;DK1
Katowice;Kielce;154;02:17;DK78
Katowice;Gliwice;30;00:28;A4
Katowice;Bielsko-Biała;60;00:51;DK86 - DK1
Białystok;Płock;269;03:29;DK8
Białystok;Siedlce;153;02:16;DW678 - DK63
Białystok;Łomża;80;01:02;DK64
Częstochowa;Kielce;132;01:56;DK46 - DK78
Częstochowa;Rzeszów;271;03:38;DK78
Częstochowa;Opole;98;01:38;DK46
Częstochowa;Kalisz;160;02:22;DK12
Częstochowa;Piotrków Trybunalski;83;00:56;DK1
Częstochowa;Zamość;352;04:54;DK78 - DK74
Radom;Rzeszów;192;02:54;DK94 - S3
Radom;Płock;179;02:21;DK7 - DK50
Radom;Tarnów;184;02:53;DW756 - DK73
Radom;Piotrków Trybunalski;109;01:34;DK12
Radom;Siedlce;173;02:24;DK7 - DK50
Radom;Zamość;209;02:37;S12 - DK17
Toruń;Olsztyn;175;02:33;DK15
Toruń;Włocławek;58;00:53;DK91
Toruń;Grudziądz;65;0:48;A1
Toruń;Konin;116;02:01;DK25 - DK15
Toruń;Łomża;278;03:50;DK10 - DK60 - DK61
Kielce;Rzeszów;173;02:18;DK73 - A4
Kielce;Tarnów;118;01:54;DK73 - A4
Kielce;Nowy Sącz;173;02:43;DK75
Kielce;Piotrków Trybunalski;98;01:26;DK74
Kielce;Zamość;215;03:04;DK74
Gliwice;Opole;85;01:03;A4
Gliwice;Kalisz;194;03:01;DW901 - DW450
Rzeszów;Tarnów;86;00:57;A4
Rzeszów;Zamość;144;02:10;DW858
Olsztyn;Elbląg;111;01:19;DK16 - S7
Olsztyn;Płock;178;02:19;S7 - DK60
Olsztyn;Włocławek;220;02:55;S7
Olsztyn;Grudziądz;136;02:08;DK16
Olsztyn;Siedlce;264;03:47;DK53 - DK60
Olsztyn;Łomża;146;02:10;DK53 - DW645
Bielsko-Biała;Nowy Sącz;154;02:45;DK28 - DK52
Gorzów Wielkopolski;Zielona Góra;111;01:11;S3
Gorzów Wielkopolski;Piła;123;01:48;DK22 - DW179
Elbląg;Płock;245;02:58;S7 - DK7
Elbląg;Grudziądz;124;01:38;DK22 - A1
Elbląg;Piła;237;03:30;DK22
Elbląg;Łomża;254;03:06;S7
Płock;Włocławek;51;00:46;DK62
Płock;Kalisz;163;02:18;DK92 - DW470
Płock;Konin;125;01:44;DK60 - DK92
Płock;Łomża;195;02:46;DK61
Opole;Kalisz;150;02:20;DK42 - DK11
Zielona Góra;Legnica;108;01:29;DK3
Zielona Góra;Jelenia Góra;144;02:18;DW297
Włocławek;Kalisz;132;01:57;DW270 - DW470
Włocławek;Konin;87;01:20;DW270 - DW266
Włocławek;Łomża;237;03:28;DK60
Tarnów;Nowy Sącz;63;01:12;DW975
Tarnów;Siedlce;323;04:43;DK79 - DK63
Koszalin;Grudziądz;213;03:05;DK25 - DW240
Koszalin;Słupsk;67;01:04;DK6
Koszalin;Piła;141;02:05;DK11
Kalisz;Legnica;173;02:45;DK36
Kalisz;Konin;57;00:52;DK25
Kalisz;Piotrków Trybunalski;130;01:46;DK12 - S8
Legnica;Jelenia Góra;56;01:06;DW364
Grudziądz;Słupsk;189;02:46;DW214 - DK20 - DW210
Grudziądz;Łomża;255;03:47;DW538 - DW645
Słupsk;Piła;175;02:29;DK21 - DK20 - DK11
Siedlce;Łomża;130;01:51;DK63
Kielce;Radom;77;01:03;S7 - DK7